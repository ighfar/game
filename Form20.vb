Imports System.Data.OleDb
Public Class Form20
    Dim Conn As OleDbConnection
    Dim da As OleDbDataAdapter
    Dim ds As DataSet
    Dim Str As String


    Sub Koneksi()
        Str = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=game.mdb"
        Conn = New OleDbConnection(Str)
        If Conn.State = ConnectionState.Closed Then Conn.Open()
    End Sub

    Private Sub Form20_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Koneksi()
        da = New OleDbDataAdapter("Select * from hasil", Conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "hasil")
        DGV.DataSource = (ds.Tables("hasil"))
    End Sub
End Class
