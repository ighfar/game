﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:2.0.50727.5485
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.Microsoft.VisualBasic.HideModuleNameAttribute()>  _
    Friend Module Resources
        
        Private resourceMan As Global.System.Resources.ResourceManager
        
        Private resourceCulture As Global.System.Globalization.CultureInfo
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("Game.Resources", GetType(Resources).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        Friend ReadOnly Property _27496478_1994235867499482_87513878_n() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("27496478_1994235867499482_87513878_n", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _27583191_1994235920832810_1114276415_n() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("27583191_1994235920832810_1114276415_n", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _5() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("5", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _6() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("6", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _61() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("61", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _62() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("62", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _6ea182649799ff4860de08bc795719e2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("6ea182649799ff4860de08bc795719e2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _7() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("7", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _71() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("71", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _8() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("8", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property _81() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("81", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property AR02() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("AR02", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property AR021() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("AR021", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property AR022() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("AR022", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property AR03() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("AR03", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property AR23() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("AR23", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property AR231() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("AR231", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property AR232() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("AR232", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property AR233() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("AR233", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property AR36() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("AR36", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property bbm_outgoing_call() As System.IO.UnmanagedMemoryStream
            Get
                Return ResourceManager.GetStream("bbm_outgoing_call", resourceCulture)
            End Get
        End Property
        
        Friend ReadOnly Property dd() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("dd", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property download() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("download", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property downloadl() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("downloadl", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property es_krim_walls() As Byte()
            Get
                Dim obj As Object = ResourceManager.GetObject("es_krim_walls", resourceCulture)
                Return CType(obj,Byte())
            End Get
        End Property
        
        Friend ReadOnly Property Fantasy_Game_Loop() As System.IO.UnmanagedMemoryStream
            Get
                Return ResourceManager.GetStream("Fantasy_Game_Loop", resourceCulture)
            End Get
        End Property
        
        Friend ReadOnly Property gambar_kartun_anak_sekolah1() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("gambar-kartun-anak-sekolah1", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property high_priority() As System.IO.UnmanagedMemoryStream
            Get
                Return ResourceManager.GetStream("high_priority", resourceCulture)
            End Get
        End Property
        
        Friend ReadOnly Property hqdefaultl() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("hqdefaultl", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property images() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("images", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property p() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("p", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property sss() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("sss", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property voice_recording_start() As System.IO.UnmanagedMemoryStream
            Get
                Return ResourceManager.GetStream("voice_recording_start", resourceCulture)
            End Get
        End Property
        
        Friend ReadOnly Property voice_recording_stop() As System.IO.UnmanagedMemoryStream
            Get
                Return ResourceManager.GetStream("voice_recording_stop", resourceCulture)
            End Get
        End Property
        
        Friend ReadOnly Property WhatsApp_Image_2018_03_30_at_13_48_33() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("WhatsApp Image 2018-03-30 at 13.48.33", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property WhatsApp_Image_2018_03_30_at_13_48_34() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("WhatsApp Image 2018-03-30 at 13.48.34", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property WhatsApp_Image_2018_03_30_at_13_48_35() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("WhatsApp Image 2018-03-30 at 13.48.35", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property WhatsApp_Image_2018_03_30_at_13_48_36() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("WhatsApp Image 2018-03-30 at 13.48.36", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property WhatsApp_Image_2018_03_30_at_13_481() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("WhatsApp Image 2018-03-30 at 13.481", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        Friend ReadOnly Property WhatsApp_Image_2018_04_03_at_11_29_36() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("WhatsApp Image 2018-04-03 at 11.29.36", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
    End Module
End Namespace
